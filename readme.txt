******
*
*  September 2017
*  Author: yochaim, bYO Innovations Ltd.
*  website: mwaccelerator.sourceforge.net
*  Copyright � 2005-2017 bYO Innovations Ltd, All Rights Reserved.
*
*  
******************************************************************
		Mouse Wheel Accelerator 1.4.8
******************************************************************


* MWA improves the wheel scroll in windows towards dynamic smooth iPhone-like scrolling.
Getting the mouse wheel to accelerate intuitively still leaves the problem that some application 
do not support smooth line scroll while other do not handle well repetitive scrolling. 
This means that in spite of all the best intentions the final effect is not perfect...
still it is an improvement!!! 

* When scrolling in application window the context menu will offer the relevant 
Options:

Disable � Disable MWA
Bypass in app [*] � ignore in application [*]
Bypass for wnd [*] - ignore for all windows of class [*]
Run on startup � run on login with normal user credentials. For administrator credentials 
	use scheduled login task and leave unchecked. 
About
Exit

* Double-click icon to disable/enable.

* One notch turn in the opposite direction will abort the scroll sequence.

* Works best with applications built for smooth scrolling like browsers and image editing software.

* Works with touchpad scrolling.

* Some tested configurations:

Firefox � enable smooth scrolling and you�re set to go.
Chrome � use SmoothScroll extension with Stride size per scroll in pixel set to 15.
IE8 � there is a scroll jittering problem when the mouse is above the content area. Keep the mouse 
	above the scroll panel (or where there is no text) using smooth scroll and it�s not too bad...
Acrobat Reader -  use version 9.*, set page-display to single continuous.

IMPORTANT NOTICE:
in windows 7 64 bit KB2752274 hotfix needs to be installed!!! (see http://support.microsoft.com/kb/2752274)
  

** For more information and support please visit http://mwaccelerator.sourceforge.net


******************************************************************
version 1.4.4 December 2010

- Added handling for high-resolution scroll wheels and touchpads.


******************************************************************
version 1.4.5 July 2011

- Added reset acceleration when pressing Ctrl/Shift while scrolling.

******************************************************************
version 1.4.7 February 2015

- Added double-click icon to disable/enable.
- win 64 bit compatibility.

******************************************************************
version 1.4.8 September 2017

- Fixed compatibility with windows apps.